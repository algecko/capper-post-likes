const express = require('express')
const app = express()
const passport = require('passport')
const cors = require('cors')
const config = require('./config')
const db = require('./db')
const bodyParser = require('body-parser')
const postDal = require('./postDal')
const likeDal = require('./likeDal')
const jwtTools = require('./jwtTools')

const JwtStrategy = require('passport-jwt').Strategy
ExtractJwt = require('passport-jwt').ExtractJwt

passport.use(new JwtStrategy({
	secretOrKeyProvider: jwtTools.getSecret,
	jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
}, (jwt_payload, done) => {done(null, jwt_payload.sub)}))

const port = process.env.PORT || config.port
const dbUrl = process.env.DB_URL || config.dbUrl

app.use(passport.initialize())
app.use(cors({
	origin: process.env.NODE_ENV === 'production' ? /(https?:\/\/opentgc\.com)/ : '*'
}))
app.use(bodyParser.json())

app.use(passport.authenticate('jwt', {session: false}))

app.put('/like/:id', (req, res, next) => {
	postDal.find(req.params.id)
	.then(post => {
		if (!post)
			return res.status(404).send()

		likeDal.like(req.params.id, req.user, post.date)
		.then(() => {
			res.status(201).send()
		})
	})
	.catch(next)
})

app.put('/unlike/:id', (req, res, next) => {
	likeDal.unlike(req.params.id, req.user)
	.then(() => {
		res.status(201).send()
	})
	.catch(next)
})

app.get('/likes', (req, res, next) => {
	likeDal.getLikes(req.user)
	.then(likes => {
		res.json(likes.map(like => like.postId))
	})
	.catch(next)
})

app.use((req, res) => {
	res.status(404).send('Page not Found')
})

app.use((err, req, res, next) => {
	const {extMessage, message, statusCode = 500} = err

	const logMessage = message || extMessage
	if (logMessage)
		console.error(logMessage)

	const myResponse = {}
	if (extMessage)
		myResponse.message = extMessage

	res.status(statusCode).json(myResponse)
})

db.connect(dbUrl)
.then(() => {
	app.listen(port, () => {
		console.log(`Started at port ${port}`)
	})
})
.catch(err => {
	console.error(err)
	process.exit(1)
})