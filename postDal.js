const db = require('./db')
const getCollection = () => db.get().collection('posts')

module.exports.find = (postID) => getCollection().findOne({id: postID})
