const MongoClient = require('mongodb').MongoClient

const state = {
	client: null
}

exports.connect = function (url) {
	return new Promise((resolve, reject) => {
		if (state.client) return resolve()

		MongoClient.connect(url, function (err, client) {
			if (err) return reject(err)
			state.client = client
			resolve()
		})
	})
}

exports.get = function () {
	return state.client.db()
}

exports.close = function () {
	return new Promise((resolve, reject) => {
		if (state.client) {
			state.client.close(function (err, result) {
				if (err) return reject(err)
				state.client = null
				state.mode = null
				resolve(result)
			})
		}
	})
}