const db = require('./db')
const getCollection = () => db.get().collection('likes')

module.exports.like = function (postId, user, postDate) {
	return getCollection().update({postId: postId, user: user}, {
		postId,
		user,
		postDate,
		date: new Date()
	}, {upsert: true})
}
module.exports.unlike = function (postId, user) {
	return getCollection().deleteOne({postId: postId, user: user})
}

module.exports.getLikes = function (user) {
	return new Promise((resolve, reject) => {
		return getCollection().find({user: user}).toArray((err, docs) => {
			if (err)
				return reject(err)
			resolve(docs)
		})
	})
}