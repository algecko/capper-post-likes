const fetch = require('node-fetch')

const config = require('./config')

let cachedSecret

function getSecret (request, rawJwtToken, done) {
	if (cachedSecret)
		return done(null, cachedSecret)

	fetch(`${config.authUrl}/publicKey`)
	.catch(() => done({statusCode: 500, extMessage: 'Auth service can not be reached'}))
	.then(res => res.buffer())
	.then(secret => {
		cachedSecret = secret
		done(null, cachedSecret)
	})
	.catch(done)
}

module.exports = {getSecret}